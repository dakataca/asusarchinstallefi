#!/bin/bash

setfont Lat2-Terminus16
#setfont ter-v32n
timedatectl set-ntp true
timedatectl set-timezone America/Bogota
timedatectl status
sed -i "s/#es_CO.UTF-8 UTF-8/es_CO.UTF-8 UTF-8/" /etc/locale.gen
locale-gen
export LANG=es_CO.UTF-8
echo $LANG
timedatectl status
cfdisk /dev/sda
lsblk
mkfs.ext4 -F /dev/sda6
mkfs.ext4 -F /dev/sda7
mkfs.ext4 -F /dev/sda8
mkswap /dev/sda6
swapon /dev/sda6
mount /dev/sda7 /mnt
mkdir -p /mnt/boot/efi
mkdir -p /mnt/home
mount /dev/sda2 /mnt/boot/efi
mount /dev/sda8 /mnt/home
lsblk
nano /etc/pacman.d/mirrorlist
pacstrap /mnt base base-devel vim git
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt git clone https://gitlab.com/danieldakataca/asusarchinstallefiscript.git
arch-chroot /mnt /bin/bash
#arch-chroot /mnt /bin/bash
#mv  ../{c*/c*,p*}.sh /mnt
#mv  ../{c*/,p*/} /mnt
#rm -rf ../{c*,p*}
