rm /etc/localtime
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock -w 
sed -i "s/#es_CO.UTF-8 UTF-8/es_CO.UTF-8 UTF-8/" /etc/locale.gen
locale-gen
nano /etc/pacman.conf
#nano /etc/pacman.d/mirrorlist
echo $LANG
export LANG=es_CO.UTF-8
echo LANG=es_CO.UTF-8> /etc/locale.conf
echo KEYMAP=la-latin1> /etc/vconsole.conf
echo Arch> /etc/hostname
pacman -Syu grub os-prober dialog wpa_supplicant dhclient ifplugd ppp openvswitch iw efibootmgr xf86-input-libinput  --needed --noconfirm
grub-install --efi-directory=/boot/efi --bootloader-id=Grub --target=x86_64-efi --recheck
grub-mkconfig -o /boot/grub/grub.cfg 
mkinitcpio -p linux
echo "Digite contreseña usuario root: "
passwd
read -p "Digite nombre de usuario: " USER_ARCH
useradd -m -g users -G audio,lp,optical,storage,video,wheel,games,power,scanner,input -s /bin/bash $USER_ARCH
echo "Digite contraseña del usuario $USER_ARCH :"
passwd $USER_ARCH
nano /etc/sudoers
systemctl enable dhcpcd.service

# Instalación del escritorio
echo "--------------------Instalando escritorio----------------------------------"
pacman -Sy xorg-server xorg-xinit xorg-apps xorg-twm xorg-xclock xfce4 xfce4-goodies xdg-user-dirs  networkmanager network-manager-applet lightdm lightdm-gtk-greeter intel-ucode xf86-video-intel ttf-dejavu ttf-hack ttf-ubuntu-font-family ttf-roboto neofetch firefox firefox-i18n-es-mx chromium pulseaudio pulseaudio-alsa pulseaudio-jack libcanberra-pulse pavucontrol pulseaudio-equalizer-ladspa gvfs gvfs-mtp gvfs-gphoto2 gvfs-afc gnome-usage htop redshift mpv arc-solid-gtk-theme arc-gtk-theme arc-icon-theme papirus-icon-theme breeze breeze-gtk breeze-icons ffmpegthumbnailer fuse3 fuseiso gnome-keyring unrar p7zip zip unzip arj unarj unace lzop lzip cpio lrzip xmlto xarchiver-gtk2 alsa-utils freeglut mesa-demos bluez bluez-utils android-tools android-udev jdk-openjdk ntfs-3g gst-libav cmake bamf nfs-utils nilfs-utils ntp openconnect openvpn partclone partimage crda darkhttpd ddrescue dnsmasq dnsutils ethtool exfat-utils f2fs-tools fsarchiver gnu-netcat grml-zsh-config hdparm ipw2100-fw ipw2200-fw irssi lftp linux-atm lsscsi mc mtools ndisc6 pptpclient refind-efi rp-pppoe rsync sdparm smartmontools tcpdump testdisk usb_modeswitch vpnc wireless-regdb wvdial xl2tpd lsof bdf-unifont  telegram-desktop --needed --noconfirm
systemctl enable lightdm.service
xdg-user-dirs-update
systemctl enable dhcpcd.service
systemctl start dhcpcd.service
w=$(ls /sys/class/net | grep "^w")
systemctl disable netctl-auto@$w.service
systemctl enable NetworkManager.service
systemctl start NetworkManager.service
