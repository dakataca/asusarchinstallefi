#!/bin/bash

# Configuración
echo "--------------------Configurando sistema----------------------------------"
localectl set-x11-keymap latam,us pc103 deadtilde,intl grp:alt_shift_toggle
echo -e '--vo=gpu
--term-status-msg="Time:${time-pos}"
--alang=es,en
--slang=es,en
--vlang=es,en
--aid=auto
--ad-lavc-threads=0
--audio-pitch-correction=yes
--audio-channels=auto-safe
--audio-display=attachment
--sub-scale-with-window=yes
--screenshot-directory=~/Imágenes/mpv-screenshots
--screenshot-jpeg-quality=100
--vd-lavc-dr=yes
--deinterlace=no
--hwdec=auto
## no cierra el mpv al terminar un vídeo.
#keep-open=yes
## Reproduce archivos en orden aleatorio.
# --shuffle
## Iniciar el reproductor en estado de pausa.
#--pause
## Título de la ventana
#--title="título de ventana"
## Redimencionar ventana agregando barras negras donde sea necesario.
#--no-keepaspect
## Desactiva el vídeo y reproduce solo el audio sin abrir ninguna ventana de mpv
#--vid=no
--term-osd-bar
#profile=gpu-hq
#scale=ewa_lanczossharp
#cscale=ewa_lanczossharp
## Interpolación
#video-sync=display-resample
#interpolation
#tscale=oversample'> /etc/mpv/mpv.conf

# Configurar los presets
mkdir -p /home/$USER/.config/pulse/presets
cp ../presets/* /home/$USER/.config/pulse/presets/


<<COMMENT
#Configurar Touchpad
DEV_NAME=(sudo libinput list-devices | grep -i 'touchpad' | awk '{print $2}')


sudo echo -e 'Section "InputClass"
Identifier "${DEV_NAME}"
Driver "libinput"
	Option "Tapping" "true"
	Option "NaturalScrolling" "false"
	Option "SendEventsMode" "disabled-on-external-mouse"
	Option "ScrollMethod" "edge"
	Option "MiddleEmulation" "true"
	Option "DisableWhileTyping" "true"
	Option "AccelSpeed" "0.2"
	Option "TappingDragLock" "true"
	Option "TappingButtonMap" "lmr"
EndSection'> /etc/X11/xorg.conf.d/30-touchpad.conf


#Conectarse a la red

W=$(ls /sys/class/net | grep "^w")
sudo nmcli dev wifi list
read -p "Digite SSID de la red a la que se quiere conectar: " RED
SSID=$(sudo nmcli dev wifi list | grep -i $RED | awk '{print $1}')
PERFIL=$SSID
sudo nmcli con add con-name $PERFIL ifname $W type wifi ssid $SSID
sudo nmcli con modify $PERFIL wifi-sec.key-mgmt wpa-psk
read -sp "Digite clave de su red $SSID: " CLAVE
#echo "Digite clave de su red $SSID: "
sudo nmcli con modify $PERFIL wifi-sec.psk $CLAVE
sudo nmcli connection up $PERFIL
COMMENT

