#!/bin/bash

pacman -Sy frei0r-plugins libreoffice-fresh-es uget qbittorrent  vlc simplescreenrecorder audacity blueberry eclipse-java epdfview galculator-gtk2 flameshot gimp peek youtube-dl tvtime imagemagick handbrake acetoneiso2 seahorse kid3 kdenlive rhythmbox gucharmap speedtest-cli gnome-disk-utility inkscape cmus dmidecode code mediainfo picard gnome-boxes  --needed --noconfirm
